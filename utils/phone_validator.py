from django.core.validators import RegexValidator

phone_validator = RegexValidator(
    regex=r'^7\d{10}$',
    message='not correct phone number'
)