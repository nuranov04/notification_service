import requests
import os
import pytz
from celery.utils.log import get_task_logger
from datetime import datetime

from apps.distribution.models import Distribution
from apps.customer.models import Customer
from notification_service.celery import app

OPENAPI = os.getenv("OPENAPI")
JWT_TOKEN = os.getenv('JWT_TOKEN')

HEADER = {
    'Authorization': f'Token {JWT_TOKEN}',
    'Content-Type': 'application/json'
}

logger = get_task_logger(__name__)


@app.task(bind=True, retry_backoff=True)
def sending(self, customer_id, distribution_id, data):
    print('in sending')
    customer = Customer.objects.get(pk=customer_id)
    distribution = Distribution.objects.get(pk=distribution_id)
    customer_timezone = pytz.timezone(customer.select_timezone)
    timezone_now = datetime.now(customer_timezone)
    print('artur hello')
    if distribution.time_start <= timezone_now.time() >= distribution.time_end:
        try:
            requests.post(url=OPENAPI, headers=HEADER, json=data)
            print('ok post')
            logger.info('ok post')
        except requests.exceptions.RequestException as e:
            logger.error('message error')
            raise self.retry(e=e)
