Чтобы запустить проект вам необходимо выполнить следущие действия:

-git clone celery https://gitlab.com/nuranov04/notification_service

-cd notification_service

-python3 -m venv env

-source env/bin/activate

- pip intall -r requerements.txt

-./manage.py migrate

- celery -A notification_service worker -l info

-./manage.py runserver
