import pytz
from django.db import models
from utils.phone_validator import phone_validator

TIMEZONE_CHOICE = tuple(zip(pytz.all_timezones, pytz.all_timezones))


class Customer(models.Model):
    phone_number = models.CharField(
        max_length=11,
        validators=[phone_validator],
        unique=True
    )
    code_mobile_operator = models.CharField(
        max_length=3
    )
    tag = models.CharField(
        max_length=50
    )
    select_timezone = models.CharField(
        choices=TIMEZONE_CHOICE,
        max_length=40
    )

    def __str__(self):
        return f"{self.id}"

