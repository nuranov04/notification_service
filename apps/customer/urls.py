from rest_framework.routers import DefaultRouter


from apps.customer.views import CustomerViewSet

router = DefaultRouter()
router.register(
    prefix="customer",
    viewset=CustomerViewSet
)

urlpatterns = router.urls