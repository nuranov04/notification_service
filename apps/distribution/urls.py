from rest_framework.routers import DefaultRouter

from apps.distribution.views import DistributionViewSet

router = DefaultRouter()
router.register(
    prefix='distribution',
    viewset=DistributionViewSet
)

urlpatterns = router.urls