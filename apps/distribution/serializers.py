from rest_framework import serializers
from django.db.models import Q

from apps.distribution.models import Distribution
from apps.customer.models import Customer
from apps.message.models import Message
from utils.celery_task import sending


class DistributionSerializer(serializers.ModelSerializer):
    class Meta:
        model = Distribution
        fields = "__all__"

    def create(self, validated_data):
        distribution = Distribution.objects.create(**validated_data)
        costumers = Customer.objects.filter(
            Q(code_mobile_operator=distribution.mobile_operator_code) |
            Q(tag=distribution.tag)).all()
        for costumer in costumers:
            Message.objects.create(
                distribution_id=distribution.id,
                costumer_id=costumer.id,
                status='No'
            )
            message = Message.objects.filter(
                distribution_id=distribution.id,
                costumer_id=costumer.id
            ).first()
            data = {
                'id': message.id,
                "phone": costumer.phone_number,
                "text": distribution.text
            }
            costumer_id = costumer.id
            distribution_id = distribution.id
            print('sending')
            sending.delay(customer_id=costumer_id, distribution_id=distribution_id, data=data)
        return distribution
