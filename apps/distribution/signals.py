from django.dispatch import receiver
from django.db.models.signals import post_save
from django.db.models import Q

from apps.distribution.models import Distribution
from apps.message.models import Message
from apps.customer.models import Customer
from utils.celery_task import sending


@receiver(post_save, sender=Distribution, dispatch_uid="message_for_users")
def message_for_send(sender, instance, **kwargs):
    distribution = Distribution.objects.filter(id=instance.id).first()
    costumers = Customer.objects.filter(
        Q(code_mobile_operator=distribution.mobile_operator_code) |
        Q(tag=distribution.tag)).all()

    for costumer in costumers:
        Message.objects.create(
            distribution_id=distribution.id,
            costumer_id=costumer.id,
            status="No"
        )
        message = Message.objects.filter(distribution_id=instance.id, costumer_id=costumer.id).first()
        json_data = {
            'id': message.id,
            'phone': costumer.phone_number,
            'text': distribution.text
        }
        customer_id = costumer.id
        distribution_id = distribution.id
        sending.apply_async(
            customer_id=customer_id,
            distribution_id=distribution_id,
            data=json_data
        )
        print('ok send')
