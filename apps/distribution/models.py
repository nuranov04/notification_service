from django.db import models


class Distribution(models.Model):
    date_of_start = models.DateTimeField()
    date_of_end = models.DateTimeField()
    time_start = models.TimeField()
    time_end = models.TimeField()
    text = models.TextField()
    tag = models.CharField(
        max_length=50
    )
    mobile_operator_code = models.CharField(
        max_length=4
    )

    def __str__(self):
        return f"{self.id}"



