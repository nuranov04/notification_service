from django.shortcuts import get_object_or_404
from rest_framework import viewsets
from rest_framework.response import Response
from rest_framework.decorators import action

from apps.distribution.serializers import DistributionSerializer
from apps.distribution.models import Distribution
from apps.message.serializers import MessageSerializer
from apps.message.models import Message


class DistributionViewSet(viewsets.ModelViewSet):
    queryset = Distribution.objects.all()
    serializer_class = DistributionSerializer

    # statistics for distribution
    @action(detail=True, methods=['get'])
    def statistics(self, request, pk=None):
        distributions = Distribution.objects.all()
        get_object_or_404(distributions, pk=pk)
        queryset = Message.objects.filter(distribution_id=pk).all()
        serializer = MessageSerializer(queryset, many=True)
        return Response(serializer.data)

    # statistics for all distributions
    @action(detail=False, methods=['get'])
    def full_statistics(self, request):
        """
        Summary data for all mailings
        """
        total_count = Distribution.objects.count()
        mailing = Distribution.objects.values('id')
        content = {'Total number of mailings': total_count,
                   'The number of messages sent': ''}
        result = {}

        for row in mailing:
            res = {'Total messages': 0, 'Sent': 0, 'No sent': 0}
            mail = Message.objects.filter(distribution=row['id']).all()
            group_sent = mail.filter(status='OK')
            group_no_sent = mail.filter(status='NO')
            res['Total messages'] = len(mail)
            res['Sent'] = len(group_sent)
            res['No sent'] = len(group_no_sent)
            result[row['id']] = res

        content['The number of messages sent'] = result
        return Response(content)