from rest_framework.routers import DefaultRouter

from apps.message.views import MessageViewSet

router = DefaultRouter()
router.register(
    prefix='message',
    viewset=MessageViewSet
)

urlpatterns = router.urls
