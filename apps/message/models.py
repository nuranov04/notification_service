from django.db import models
from apps.distribution.models import Distribution
from apps.customer.models import Customer

MESSAGE_STATUS_CHOICE = (
    ('OK', 'OK'),
    ('NO', 'NO')
)


class Message(models.Model):
    create_at = models.DateTimeField(
        auto_now_add=True
    )
    distribution = models.ForeignKey(
        Distribution,
        on_delete=models.CASCADE,
        related_name='distribution'
    )
    costumer = models.ForeignKey(
        Customer,
        on_delete=models.CASCADE,
        related_name='customer'
    )
    status = models.CharField(
        choices=MESSAGE_STATUS_CHOICE,
        max_length=5
    )

    def __str__(self):
        return f"{self.consumer}--{self.distribution}"
