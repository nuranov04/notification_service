from rest_framework import viewsets

from apps.message.models import Message
from apps.message.serializers import MessageSerializer


class MessageViewSet(viewsets.ModelViewSet):
    queryset = Message.objects.all()
    serializer_class = MessageSerializer
